import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import library.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ExeStartCProcedure implements Plugin {
    @Override
    public List<String> getResult(String cfId, String urlToControlSystem /*http://localhost:12345/*/) {
        JsonNode response = null;
        String exePath = null;

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(urlToControlSystem + "/" + cfId + "/info?type=full").openConnection();
            connection.setRequestMethod("GET");

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            reader.close();

            ObjectMapper objectMapper = new ObjectMapper();
            response = objectMapper.readTree(builder.toString());

            JsonNode pluginSpecificArray = response.get("pluginSpecific");
            if (pluginSpecificArray != null && pluginSpecificArray.isArray()) {
                for (JsonNode pluginNode : pluginSpecificArray) {
                    if ("ExeStartCProcedure".equals(pluginNode.get("name").asText())) {
                        exePath = pluginNode.get("exe_path").asText();
                        break;
                    }
                }
            }

            if (exePath != null) {
                System.out.println("Exe Path found: " + exePath);
            } else {
                System.out.println("Exe Path for ExeStartCProcedure not found.");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        if (response == null) {
            System.out.println("Can't get access to controlSystem");
            return new ArrayList<>();
        }

        JsonNode errorNode = response.get("errorOccurred");
        boolean error = errorNode != null && errorNode.asBoolean();
        if (error) {
            System.out.println("Error accessing the control system: " + response.asText());
            return new ArrayList<>();
        }

        int inputCount = response.get("inputVarCount") != null ? response.get("inputVarCount").asInt() : 0;
        int outputCount = response.get("outputVarCount") != null ? response.get("outputVarCount").asInt() : 0;

        List<String> input_types = new ArrayList<>();
        List<String> output_types = new ArrayList<>();

        JsonNode input = response.get("inputVarTypes");
        if (input != null && input.isArray()) {
            for (JsonNode node : input) {
                input_types.add(node.asText());
            }
        }

        JsonNode output = response.get("outputVarTypes");
        if (output != null && output.isArray()) {
            for (JsonNode node : output) {
                output_types.add(node.asText());
            }
        }

        String include = "#include <cstdlib>\n#include <cstring>\n#include <cstdio>\n";
        String name = "exeStartCProcedure";

        StringBuilder main = new StringBuilder();
        StringBuilder mainString = new StringBuilder();
        mainString.append("int ").append("exeStartCProcedure(");

        for (int i = 0; i < inputCount; i++) {
            String type = input_types.get(i).split(";")[0];
            if ("int".equals(type)) {
                mainString.append("const int i").append(i);
            } else if ("local_path".equals(type) || "any_path".equals(type)) {
                mainString.append("const char* i").append(i);
            }
            if (i < inputCount - 1 || outputCount > 0) {
                mainString.append(", ");
            }
        }

        for (int i = 0; i < outputCount; i++) {
            String type = output_types.get(i).split(";")[0];
            if ("int".equals(type)) {
                mainString.append("int* o").append(i);
            } else if ("local_path".equals(type) || "any_path".equals(type)) {
                mainString.append("char* o").append(i);
            }
            if (i < outputCount - 1) {
                mainString.append(", ");
            }
        }

        mainString.append(")");
        main.append(mainString);
        main.append("\n{\nchar exe[2048] = \"./src/").append(exePath).append("\";\n");

        for (int i = 0; i < inputCount; i++) {
            String type = input_types.get(i).split(";")[0];
            main.append("strcat(exe, \" \");\n");
            if ("int".equals(type)) {
                main.append("char str").append(i).append("[25];\n");
                main.append("sprintf(str").append(i).append(", \"%d\", i").append(i).append(");\n");
                main.append("strcat(exe, str").append(i).append(");\n");
            } else if ("local_path".equals(type) || "any_path".equals(type)) {
                main.append("strcat(exe, i").append(i).append(");\n");
            }
        }

        main.append("int check = system(exe);\nif(check == 1) return 1;\n");

        for (int i = 0; i < outputCount; i++) {
            String type = output_types.get(i).split(";")[0];
            if ("local_path".equals(type) || "any_path".equals(type)) {
                String path = response.get("localPaths").get(i).asText();
                main.append("sprintf(o").append(i).append(", \"").append(path).append("\");\n");
            }
        }

        main.append("return 0;\n}\n");

        List<String> result = new ArrayList<>();
        result.add(name);
        result.add(include);
        result.add(main.toString());

        return result;
    }
}