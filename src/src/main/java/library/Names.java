package library;

public class Names {
    static final String FRAGMENTS_FOLDER = "code_fragments";
    static final String SOURCE_FOLDER = "src";
    static final String DESCRIPTION_FILE = "description.xml";
    static final String TYPE_EXE_START_C_PROCEDURE = "ExeStartCProcedure";
    static final String TYPE_LuNA = "LuNA_sub";
    static final String PLUGINS_FOLDER = "plugins";
    static final String MAIN_FOLDER = "codeFragmentControlSystem";
    static final String URL = "http://localhost:12345";
}
