package library;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jdom2.Element;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.lang.System;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

import static library.Names.*;
import static library.Utils.*;

import library.exceptions.*;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

@SpringBootApplication
@RestController
public class ModuleLibrary {
    private static final Path cDrive = Paths.get("C:/");
    private static final Path folderPath = cDrive.resolve(MAIN_FOLDER);
    private static final Path pluginPath = getPluginPath(folderPath);
    private static final Path fragmentsPath = getFragmentsPath(folderPath);

    static Path getPluginPath(Path folderPath) {
        var path = System.getenv("CFCS_PLUGINS_PATH");
        if (path != null) {
            return Paths.get(path);
        } else {
            return folderPath.resolve(PLUGINS_FOLDER);
        }
    }

    static Path getFragmentsPath(Path folderPath) {
        var path = System.getenv("CFCS_FRAGMENTS_PATH");
        if (path != null) {
            return Paths.get(path);
        } else {
            return folderPath.resolve(FRAGMENTS_FOLDER);
        }
    }

    public static void main(String[] args) {
        System.out.println("Starting application...");
        if (initFolders()) {
            System.out.println("Plugins path: " + pluginPath);
            System.out.println("Fragments path: " + fragmentsPath);
            SpringApplication.run(ModuleLibrary.class, args);
        }
    }

    @GetMapping("/{unknownPath}")
    public ResponseEntity<?> handleNotFound(@PathVariable String unknownPath) {
        System.out.println("HandleNotFound routing");
        Map<String, Object> errorResponse = new HashMap<>();
        errorResponse.put("errorOccurred", true);
        errorResponse.put("errorMessage", "No such method: " + unknownPath + " is implemented");
        errorResponse.put("errorCode", 10);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @GetMapping(value = "/{className}/{unknownPath}")
    public ResponseEntity<?> handleClassName(@PathVariable String className, @PathVariable String unknownPath) {
        System.out.println("Unknown codeFragmentMethod / Code Fragment routing");
        ResponseEntity<Map<String, Object>> list = getCodeFragments();
        if (list == null || list.getBody() == null) {
            return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentException("There is no such code_fragment in the system right now: " + className)));
        }
        List<String> codeFragments = (List<String>) list.getBody().get("code_fragments");
        if (codeFragments == null || codeFragments.isEmpty()) {
            return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentException("There are no code fragments available in the system.")));
        }
        if (!codeFragments.contains(className)) {
            return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentException("There is no such code_fragment in the system right now: " + className)));
        }
        return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentMethodException("There is no such method in the requested code fragment: " + unknownPath)));
    }

    @GetMapping(value = "/code_fragments", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getCodeFragments() {
        System.out.println("code fragments routing");
        Map<String, Object> resultJson = new HashMap<>();
        List<String> result = new ArrayList<>();

        try {
            if (Files.exists(fragmentsPath)) {
                try (Stream<Path> paths = Files.walk(fragmentsPath, 1)) {
                    paths.filter(Files::isDirectory).skip(1).forEach(packageDir -> {
                        Path descriptionPath = packageDir.resolve("description.xml");
                        try {
                            List<String> ids = DescriptionParser.getCodeFragmentIds(descriptionPath);
                            result.addAll(ids);
                        } catch (Exception e) {
                            System.out.println("Error parsing description.xml in package: " + packageDir.getFileName());
                        }
                    });
                }

                resultJson.put("code_fragments", result);
            } else {
                System.out.println("Fragments folder not found when trying to Files.exists(fragmentsPath)");
                return ResponseEntity.status(500).body(handleException(new WalkingCodeFragmentsFolderException("Fragments folder not found")));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(500).body(handleException(new WalkingCodeFragmentsFolderException(e.getMessage())));
        }

        return ResponseEntity.ok(resultJson);
    }

    @GetMapping(value = "/{className}/plugins", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getPluginsFromCodeFragment(@PathVariable String className) {
        System.out.println("plugins inside code fragment routing");
        Map<String, Object> resultJson = new HashMap<>();
        try {
            Element cfElement = DescriptionParser.findCodeFragmentById(fragmentsPath, className, null);
            if (cfElement != null) {
                List<String> plugins = DescriptionParser.getListOfItems(cfElement, "implemented_plugins", "plugin");
                if (!plugins.isEmpty()) {
                    resultJson.put("plugins", plugins);  // Add plugins to the result
                } else {
                    return ResponseEntity.status(404).body(handleException(new CantFindSpecificValueException("No plugins found in the field IMPLEMENTED_PLUGINS.")));
                }
            } else {
                return ResponseEntity.status(404).body(handleException(new CantFindSpecificValueException("Code fragment not found for the given class: " + className)));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(500).body(handleException(new WalkingCodeFragmentsFolderException(e.getMessage())));
        }
        return ResponseEntity.ok(resultJson);
    }

    @GetMapping(value = "/plugins", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getPlugins() {
        System.out.println("all plugins routing");
        Map<String, Object> resultJson = new HashMap<>();
        List<String> result = new ArrayList<>();

        try {
            if (Files.exists(pluginPath)) {
                try (Stream<Path> paths = Files.walk(pluginPath, 1)) {
                    paths.filter(Files::isDirectory)
                            .skip(1)
                            .forEach(folder -> result.add(folder.getFileName().normalize().toString()));
                }
            } else {
                return ResponseEntity.status(500).body(handleException(new PathToPluginFolderException("Plugins folder does not exist")));
            }
            resultJson.put("plugins", result);
        } catch (Exception e) {
            return ResponseEntity.status(500).body(handleException(new WalkingPluginFoldersException(e.getMessage())));
        }

        return ResponseEntity.ok(resultJson);
    }

    @GetMapping(value = "/{className}/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getInfo(@PathVariable String className,
                                                       @RequestParam(required = false) String type,
                                                       @RequestParam(required = false) String specific) {
        System.out.println("Code fragment info routing");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode result = objectMapper.createObjectNode();

        try {
            StringBuilder packageName = new StringBuilder();
            Element cfElement = DescriptionParser.findCodeFragmentById(fragmentsPath, className, packageName);
            result.put("package", packageName.toString());
            if (cfElement == null) {
                return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentException("There is no such code_fragment in the system right now")));
            }

            if (specific != null) {
                String[] specificKeys = specific.split("@");
                boolean errorOccurred = false;

                for (String key : specificKeys) {
                    Object specificData = DescriptionParser.getSpecificDataFromCf(cfElement, key);
                    if (specificData != null) {
                        if (specificData instanceof List<?>) {
                            result.set(key, objectMapper.valueToTree(specificData));
                        } else if (specificData instanceof String) {
                            result.put(key, (String) specificData);
                        } else if (specificData instanceof Integer) {
                            result.put(key, (Integer) specificData);
                        } else {
                            result.put(key, specificData.toString());
                        }
                    } else {
                        errorOccurred = true;
                    }
                }

                if (errorOccurred) {
                    return ResponseEntity.status(400).body(handleException(new CantFindSpecificValueException("Some specific data not found")));
                }

                result.put("errorOccurred", false);
            } else {
                result.put("inputVarCount", (Integer) getSpecificDataOrDefault(cfElement, "inputVarCount"));
                result.put("outputVarCount", (Integer) getSpecificDataOrDefault(cfElement, "outputVarCount"));
                result.put("description", (String) getSpecificDataOrDefault(cfElement, "description"));
                result.put("name", (String) getSpecificDataOrDefault(cfElement, "name"));

                result.set("inputVarTypes", objectMapper.valueToTree(DescriptionParser.getListOfItems(cfElement, "ivar_types", "ivar_type")));
                result.set("inputVarDescriptions", objectMapper.valueToTree(DescriptionParser.getListOfItems(cfElement, "ivars", "ivar")));

                List<String> outputVarTypes = DescriptionParser.getListOfItems(cfElement, "ovar_types", "ovar_type");
                result.set("outputVarTypes", objectMapper.valueToTree(outputVarTypes));

                if ("full".equals(type)) {
                    List<String> localPaths = DescriptionParser.getListOfItems(cfElement, "local_paths", "local_path");
                    if(localPaths != null && !localPaths.isEmpty())
                    {
                        result.set("localPaths", objectMapper.valueToTree(localPaths));
                    }
                    List<Map<String, String>> pluginSpecificDetails = DescriptionParser.getPluginSpecificDetails(cfElement);
                    result.set("pluginSpecific", objectMapper.valueToTree(pluginSpecificDetails));
                }

                result.set("outputVarDescriptions", objectMapper.valueToTree(DescriptionParser.getListOfItems(cfElement, "ovars", "ovar")));

                result.set("implementedPlugins", objectMapper.valueToTree(DescriptionParser.getListOfItems(cfElement, "implemented_plugins", "plugin")));
            }
            return ResponseEntity.ok(objectMapper.convertValue(result, Map.class));

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(500).body(handleException(new WalkingCodeFragmentsFolderException(e.getMessage())));
        }
    }

    @PostMapping(value = "/add_package", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> addPackage(@RequestParam String packageName, @RequestPart("xml") MultipartFile xmlFile, @RequestPart("file") MultipartFile tarFile) {
        System.out.println("AddPackage routing");
        Path fragmentPath = null;
        try {
            fragmentPath = fragmentsPath.resolve(packageName);
            createCodeFragmentFolder(fragmentPath);
            boolean validXml = processIdAndSaveXml(xmlFile, fragmentPath, fragmentsPath);
            if (!validXml) {
                deleteDirectory(fragmentPath.toFile());
                return ResponseEntity.status(400).body(Map.of("errorOccurred", true, "errorMessage", "Invalid XML format: <id> tags already present", "errorCode", 3));
            }
            extractTarFile(tarFile, fragmentPath);

        } catch (PackageAlreadyExistsException e) {
            System.out.println("AddCodeFragment routing - Code fragment already exists: " + e.getMessage());
            return ResponseEntity.status(409).body(handleException(e));
        } catch (PathToCodeFragmentException | CodeFragmentFolderCreationException | SrcTarExtractionException | IOException e) {
            System.out.println("AddCodeFragment routing - internal error: " + e.getMessage() + " " + e.getClass() + " handled: " + handleException(e));
            if(fragmentPath != null)
            {
                deleteDirectory(fragmentPath.toFile());
            }
            return ResponseEntity.status(500).body(handleException(e));
        } catch (Exception e) {
            System.out.println("AddCodeFragment routing - problems while processing xmlFile: " + e.getMessage());
            return ResponseEntity.status(409).body(handleException(e));
        }
        HashMap<String, Object> result = new HashMap<>();
        result.put("errorOccurred", false);
        System.out.println("AddCodeFragment routing success");
        return ResponseEntity.ok(result);
    }

    @DeleteMapping(value = "/remove_package", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> removeCodeFragment(@RequestParam String packageName) {
        System.out.println("RemovePackage routing");
        Path fragmentPath = fragmentsPath.resolve(packageName);

        if (!Files.exists(fragmentPath)) {
            return ResponseEntity.status(404).body(Map.of("error", "Code fragment not found."));
        }
        deleteDirectory(fragmentPath.toFile());

        HashMap<String, Object> result = new HashMap<>();
        result.put("errorOccurred", false);
        System.out.println("RemoveCodeFragment routing success");
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/{className}/pluginProcedure", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getPluginProcedure(@PathVariable String className, @RequestParam() String plugin) {
        System.out.println("Code fragment " + className + " plugin Procedure for " + plugin + " routing");
        ResponseEntity<Map<String, Object>> list = getCodeFragments();
        List<String> codeFragments = (List<String>) (list.getBody().get("code_fragments"));
        if (!codeFragments.contains(className)) {
            System.out.println("Code fragment " + className + " plugin Procedure for " + plugin + " routing fail. codeFragments does not contain className");
            return ResponseEntity.status(400).body(handleException(new UnknownCodeFragmentException("There is no such code_fragment in the system right now")));
        }

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode result = objectMapper.createObjectNode();
        switch (plugin) {
            case (TYPE_EXE_START_C_PROCEDURE): {
                JsonNode body = objectMapper.valueToTree(getExeStartCProcedure(className));
                if (checkAndAddErrorsToResult(body, result)) {
                    System.out.println("internal error in checkAndAddErrors");
                    return ResponseEntity.status(500).body(objectMapper.convertValue(body, Map.class));
                }
                System.out.println("Code fragment " + className + " plugin Procedure for " + plugin + " routing success");
                return ResponseEntity.ok(getExeStartCProcedure(className));
            }
            case (TYPE_LuNA): {
                System.out.println("idk what to do with luna yet");
                return ResponseEntity.ok(new HashMap<>());
            }
        }
        System.out.println("Code fragment " + className + " plugin Procedure for " + plugin + " routing: Unknown plugin requested (plugin code): " + plugin);
        return ResponseEntity.status(400).body(handleException(new UnknownRequestException("Unknown plugin requested. Internal plugin code requested: " + plugin)));
    }

    @GetMapping(value = "/{className}/getNeededContentFor", produces = "application/x-tar")
    public @ResponseBody ResponseEntity<?> getTargetCode(@PathVariable String className, @RequestParam() String plugin) {
        System.out.println("Code fragment " + className + " target code");
        ResponseEntity<Map<String, Object>> list = getCodeFragments();
        List<String> codeFragments = (List<String>) (list.getBody().get("code_fragments"));
        if (!codeFragments.contains(className)) {
            System.out.println("There is no such code_fragment in the system right now");
            return ResponseEntity.status(400).body("There is no such code_fragment in the system right now".getBytes());
        }

        byte[] tarResult;
        try {
            ArrayList<String> neededFiles = getNeededFilesForPlugin(plugin, className);
            tarResult = createTarFromFiles(neededFiles, DescriptionParser.findCodeFragmentPathById(fragmentsPath, className).resolve("src"));
        } catch (Exception e) {
            Map<String, Object> errorResponse = handleException(e);
            return ResponseEntity.status(500).body(errorResponse);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", className + ".tar");
        System.out.println("Code fragment " + className + " target code success");
        return new ResponseEntity<>(tarResult, headers, HttpStatus.OK);
    }

    private ArrayList<String> getNeededFilesForPlugin(String pluginName, String codeFragmentId) throws Exception
    {
        ArrayList<String> result = new ArrayList<>();

        switch (pluginName) {
            case TYPE_EXE_START_C_PROCEDURE ->
            {
                Element pluginInfo = DescriptionParser.getSpecificDataForPluginWithNameEquals(DescriptionParser.findCodeFragmentById(fragmentsPath, codeFragmentId, null), TYPE_EXE_START_C_PROCEDURE);
                if(pluginInfo == null)
                {
                    return null;
                }
                result.add(pluginInfo.getChildText("exe_path"));
            }
            default -> {
                return null;
            }
        }
        return result;
    }

    private @ResponseBody Map<String, Object> getExeStartCProcedure(@PathVariable String className) {
        System.out.println("Code fragment " + className + " getExeStartCProcedure");
        try {
            List<String> list = loadAndRunPlugin(className, TYPE_EXE_START_C_PROCEDURE);
            Map<String, Object> result = new HashMap<>();
            result.put("name", list.get(0));
            result.put("procedure", list.get(2));
            result.put("include", list.get(1));
            result.put("errorOccurred", false);
            System.out.println("Code fragment " + className + " getExeStartCProcedure sucess");
            return result;
        } catch (Exception e) {
            System.out.println("Code fragment " + className + " getExeStartCProcedure failure: " + e.getMessage() + " " + e.getCause() + " " + e.getClass());
            return handleException(e);
        }
    }

    private List<String> loadAndRunPlugin(String className, String pluginName) throws WrongPluginFormat {
        System.out.println("Code fragment " + className + " loadAndRunPlugin");

        try {
            File sourceFile = pluginPath.resolve(pluginName).resolve(pluginName + ".java").toFile();
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

            if (compiler == null) {
                throw new IllegalStateException("No Java compiler available. Make sure you're running a JDK, not a JRE.");
            }

            int compilationResult = compiler.run(null, null, null, sourceFile.getPath());
            if (compilationResult != 0) {
                throw new WrongPluginFormat("Failed to compile plugin: " + sourceFile.getName());
            }

            File classDir = pluginPath.resolve(pluginName).toFile();
            URL[] urls = new URL[]{classDir.toURI().toURL()};
            ClassLoader cl = new URLClassLoader(urls, Thread.currentThread().getContextClassLoader());

            Class<?> cls = Class.forName(pluginName, true, cl);
            Object object = cls.getDeclaredConstructor().newInstance();

            Plugin plugin = null;
            if (object instanceof Plugin) {
                plugin = (Plugin) object;
            }
            if (plugin == null) {
                return new ArrayList<>();
            }
            System.out.println("Code fragment " + className + " loadAndRunPlugin success");

            return plugin.getResult(className, URL);

        } catch (Exception e) {
            System.out.println("Code fragment " + className + " loadAndRunPlugin failure: " + e.getMessage());
            throw new WrongPluginFormat(e.getMessage());
        }
    }

    private boolean checkAndAddErrorsToResult(JsonNode check, ObjectNode result) {
        System.out.println("checkAndAddErrorsToResult");
        if (check.get("errorOccurred").asBoolean()) {
            result.put("errorOccurred", true);
            result.put("errorCode", check.get("errorCode").asBoolean());
            result.put("errorMessage", check.get("errorMessage").asBoolean());
            return true;
        }
        return false;
    }

    private Map<String, Object> handleException(Exception e) {
        Map<String, Object> resultJson = new HashMap<>();
        resultJson.put("errorOccurred", true);
        resultJson.put("errorMessage", e.getMessage());
        switch (e) {
            case C_procedurePluginAccessException cProcedurePluginAccessException -> resultJson.put("errorCode", 1);
            case DescriptionAccessException descriptionAccessException -> resultJson.put("errorCode", 2);
            case InvalidDescriptionFormatException invalidDescriptionFormatException -> resultJson.put("errorCode", 3);
            case PathToPluginFolderException pathToPluginFolderException -> resultJson.put("errorCode", 4);
            case TarFolderException tarFolderException -> resultJson.put("errorCode", 5);
            case WalkingCodeFragmentsFolderException walkingCodeFragmentsFolderException ->
                    resultJson.put("errorCode", 6);
            case WalkingPluginFoldersException walkingPluginFoldersException -> resultJson.put("errorCode", 7);
            case UnknownPluginRequested unknownPluginRequested -> resultJson.put("errorCode", 8);
            case UnknownCodeFragmentException unknownCodeFragmentException -> resultJson.put("errorCode", 11);
            case UnknownCodeFragmentMethodException unknownCodeFragmentMethodException ->
                    resultJson.put("errorCode", 10);
            case UnknownRequestException unknownRequestException -> resultJson.put("errorCode", 9);
            case WriteToDescriptionException writeToDescriptionException -> resultJson.put("errorCode", 12);
            case PackageAlreadyExistsException packageAlreadyExistsException ->
                    resultJson.put("errorCode", 13);
            case PathToCodeFragmentException pathToCodeFragmentException -> resultJson.put("errorCode", 14);
            case CodeFragmentFolderCreationException codeFragmentFolderCreationException ->
                    resultJson.put("errorCode", 15);
            case SrcTarExtractionException srcTarExtractionException -> resultJson.put("errorCode", 16);
            case CantFindSpecificValueException cantFindSpecificValueException -> resultJson.put("errorCode", 17);
            case WrongPluginFormat wrongPluginFormat -> resultJson.put("errorCode", 18);
            default -> resultJson.put("errorCode", 19);
        }
        return resultJson;
    }

    private static boolean initFolders() {
        System.out.println("Folder initialization");
        if (!Files.exists(pluginPath)) {
            try {
                Files.createDirectories(pluginPath);
                System.out.println("Plugin folder initialisation successful");
            } catch (IOException e) {
                System.err.println("Failed plugin folder initialisation: " + e.getMessage());
                return false;
            }
        }
        if (!Files.exists(fragmentsPath)) {
            try {
                Files.createDirectories(fragmentsPath);
                System.out.println("Fragments folder initialisation successful");
            } catch (IOException e) {
                System.err.println("Failed fragments folder initialisation: " + e.getMessage());
                return false;
            }
        }
        if (!Files.exists(fragmentsPath)) {
            try {
                Files.createDirectories(fragmentsPath);
                System.out.println("Fragments folder initialisation successful");
            } catch (IOException e) {
                System.err.println("Failed fragments folder initialisation: " + e.getMessage());
                return false;
            }
        }
        return true;
    }

    private void createCodeFragmentFolder(Path id) throws PathToCodeFragmentException, PackageAlreadyExistsException, CodeFragmentFolderCreationException {
        System.out.println("createCodeFragmentFolder");

        if (!Files.exists(fragmentsPath)) {
            throw new PathToCodeFragmentException("fragments folder not found");
        }

        try {
            Path newFolderPath = fragmentsPath.resolve(id);
            if (Files.exists(newFolderPath)) {
                throw new PackageAlreadyExistsException("code fragment folder already exists");
            }
            Files.createDirectory(newFolderPath);
        } catch (IOException e) {
            System.out.println("createCodeFragmentFolder failure: " + e.getMessage() + " " + e.getCause() + " " + e.getClass());
            throw new CodeFragmentFolderCreationException(e.getMessage());
        }
    }

    private Object getSpecificDataOrDefault(Element cfElement, String dataType) {
        Object data = DescriptionParser.getSpecificDataFromCf(cfElement, dataType);
        return data != null ? data : "null";
    }
}
