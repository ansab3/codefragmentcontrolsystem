package library;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DescriptionParser {
    public static List<String> getCodeFragmentIds(Path descriptionPath) throws Exception {
        List<String> ids = new ArrayList<>();
        if (Files.exists(descriptionPath)) {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(Files.newInputStream(descriptionPath));
            Element rootElement = document.getRootElement();
            List<Element> codeFragments = rootElement.getChildren("cf");

            for (Element cfElement : codeFragments) {
                String id = cfElement.getChildText("id");
                ids.add(id);
            }
        }
        return ids;
    }

    public static Element findCodeFragmentById(Path parentPath, String id, StringBuilder packageName) throws Exception {
        if (Files.exists(parentPath) && Files.isDirectory(parentPath)) {
            try {
                for (Path packageDir : Files.newDirectoryStream(parentPath)) {
                    Path descriptionPath = packageDir.resolve("description.xml");
                    if (Files.exists(descriptionPath)) {
                        SAXBuilder saxBuilder = new SAXBuilder();
                        Document document = saxBuilder.build(Files.newInputStream(descriptionPath));
                        Element rootElement = document.getRootElement();
                        List<Element> codeFragments = rootElement.getChildren("cf");

                        for (Element cfElement : codeFragments) {
                            if (id.equals(cfElement.getChildText("id"))) {
                                if(packageName != null)
                                {
                                    packageName.append(packageDir.getFileName().toString());
                                }
                                return cfElement;
                            }
                        }
                    }
                }
            } catch (IOException e) {
                throw new Exception("Error reading package directories: " + e.getMessage(), e);
            }
        }
        return null;
    }

    public static Path findCodeFragmentPathById(Path parentPath, String id) throws Exception {
        if (Files.exists(parentPath) && Files.isDirectory(parentPath)) {
            try {
                for (Path packageDir : Files.newDirectoryStream(parentPath)) {
                    Path descriptionPath = packageDir.resolve("description.xml");
                    if (Files.exists(descriptionPath)) {
                        SAXBuilder saxBuilder = new SAXBuilder();
                        Document document = saxBuilder.build(Files.newInputStream(descriptionPath));
                        Element rootElement = document.getRootElement();
                        List<Element> codeFragments = rootElement.getChildren("cf");

                        for (Element cfElement : codeFragments) {
                            if (id.equals(cfElement.getChildText("id"))) {
                                return packageDir;
                            }
                        }
                    }
                }
            } catch (IOException e) {
                throw new Exception("Error reading package directories: " + e.getMessage(), e);
            }
        }
        return null;
    }

    public static Object getSpecificDataFromCf(Element cfElement, String dataType) {
        switch (dataType) {
            case "inputVarCount":
                String inputCount = cfElement.getChildText("input_var_count");
                return inputCount != null ? Integer.parseInt(inputCount) : null;
            case "outputVarCount":
                String outputCount = cfElement.getChildText("output_var_count");
                return outputCount != null ? Integer.parseInt(outputCount) : null;
            case "description":
                return cfElement.getChildText("description");
            case "name":
                return cfElement.getChildText("name");
            case "inputVarTypes":
                return getListOfItems(cfElement, "ivar_types", "ivar_type");
            case "inputVarDescriptions":
                return getListOfItems(cfElement, "ivars", "ivar");
            case "outputVarDescriptions":
                return getListOfItems(cfElement, "ovars", "ovar");
            case "implementedPlugins":
                return getListOfItems(cfElement, "implemented_plugins", "plugin");
            case "pluginSpecific":
                return getPluginSpecificDetails(cfElement);
            default:
                return null;
        }
    }

    public static List<String> getListOfItems(Element cfElement, String parentTag, String childTag) {
        List<String> items = new ArrayList<>();
        Element parentElement = cfElement.getChild(parentTag);
        if (parentElement != null) {
            for (Element childElement : parentElement.getChildren(childTag)) {
                items.add(childElement.getText());
            }
        }
        return items;
    }

    public static Element getSpecificDataForPluginWithNameEquals(Element codeFragment, String name)
    {
        if(codeFragment == null || name == null)
        {
            return null;
        }
        Element currentElement = codeFragment.getChild("plugin_specific");
        List<Element> plugins = currentElement.getChildren();

        for (Element plugin : plugins)
        {
            if(plugin.getChild("name").getText().equals(name))
            {
                return plugin;
            }
        }
        return null;
    }

    public static String getSpecificData(Element parentElement, String... subkeys) {
        Element currentElement = parentElement;

        for (String subkey : subkeys) {
            currentElement = currentElement.getChild(subkey);
            if (currentElement == null) {
                return null;
            }
        }
        return currentElement.getText();
    }

    public static List<Map<String, String>> getPluginSpecificDetails(Element cfElement) {
        List<Map<String, String>> pluginDetailsList = new ArrayList<>();

        Element pluginSpecificElement = cfElement.getChild("plugin_specific");
        if (pluginSpecificElement != null) {
            for (Element plugin : pluginSpecificElement.getChildren("plugin")) {
                Map<String, String> pluginDetails = new HashMap<>();
                for (Element child : plugin.getChildren()) {
                    pluginDetails.put(child.getName(), child.getText());
                }
                pluginDetailsList.add(pluginDetails);
            }
        }

        return pluginDetailsList;
    }
}