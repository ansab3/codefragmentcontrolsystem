package library.exceptions;

public class WrongPluginFormat extends Exception {
    public WrongPluginFormat(String message) {
        super(message);
    }
}
