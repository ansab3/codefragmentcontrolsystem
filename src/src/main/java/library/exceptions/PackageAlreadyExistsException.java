package library.exceptions;

public class PackageAlreadyExistsException extends Exception {
    public PackageAlreadyExistsException(String message) {
        super(message);
    }
}
