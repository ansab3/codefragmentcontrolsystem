package library.exceptions;

public class UnknownCodeFragmentException extends Exception {
    public UnknownCodeFragmentException(String message) {
        super(message);
    }
}
