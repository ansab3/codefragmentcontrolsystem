package library.exceptions;

public class CantFindSpecificValueException extends Exception {
    public CantFindSpecificValueException(String message) {
        super(message);
    }
}
