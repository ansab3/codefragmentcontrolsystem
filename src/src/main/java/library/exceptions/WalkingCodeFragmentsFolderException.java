package library.exceptions;

public class WalkingCodeFragmentsFolderException extends Exception {
    public WalkingCodeFragmentsFolderException(String message) {
        super(message);
    }
}
