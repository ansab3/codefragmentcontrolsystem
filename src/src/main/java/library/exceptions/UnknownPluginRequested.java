package library.exceptions;

public class UnknownPluginRequested extends Exception {
    public UnknownPluginRequested(String message) {
        super(message);
    }
}
