package library.exceptions;

public class WriteToDescriptionException extends Exception {
    public WriteToDescriptionException(String message) {
        super(message);
    }
}
