package library.exceptions;

public class DescriptionAccessException extends Exception {
    public DescriptionAccessException(String message) {
        super(message);
    }
}
