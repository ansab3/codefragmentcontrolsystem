package library.exceptions;

public class UnknownRequestException extends Exception {
    public UnknownRequestException(String message) {
        super(message);
    }
}
