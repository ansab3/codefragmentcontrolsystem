package library.exceptions;

public class CodeFragmentFolderCreationException extends Exception {
    public CodeFragmentFolderCreationException(String message) {
        super(message);
    }
}
