package library.exceptions;

public class UnknownCodeFragmentMethodException extends Exception {
    public UnknownCodeFragmentMethodException(String message) {
        super(message);
    }
}
