package library.exceptions;

public class C_procedurePluginAccessException extends Exception {
    public C_procedurePluginAccessException(String message) {
        super(message);
    }
}
