package library.exceptions;

public class WalkingPluginFoldersException extends Exception {
    public WalkingPluginFoldersException(String message) {
        super(message);
    }
}
