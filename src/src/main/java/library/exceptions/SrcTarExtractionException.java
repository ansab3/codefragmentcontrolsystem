package library.exceptions;

public class SrcTarExtractionException extends Exception {
    public SrcTarExtractionException(String message) {
        super(message);
    }
}
