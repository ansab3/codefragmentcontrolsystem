package library.exceptions;

public class TarFolderException extends Exception {
    public TarFolderException(String message) {
        super(message);
    }
}
