package library.exceptions;

public class PathToPluginFolderException extends Exception {
    public PathToPluginFolderException(String message) {
        super(message);
    }
}
