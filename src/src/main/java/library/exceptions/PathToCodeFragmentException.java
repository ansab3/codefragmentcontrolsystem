package library.exceptions;

public class PathToCodeFragmentException extends Exception {
    public PathToCodeFragmentException(String message) {
        super(message);
    }
}
