package library;

import library.exceptions.*;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

import static library.DescriptionParser.findCodeFragmentById;

public class Utils {
    static void extractTarFile(MultipartFile tar, Path destination) throws SrcTarExtractionException {
        System.out.println("extractTarFile");
        try (TarArchiveInputStream tais = new TarArchiveInputStream(new BufferedInputStream(tar.getInputStream()))) {
            TarArchiveEntry entry;
            while ((entry = tais.getNextTarEntry()) != null) {
                Path outputPath = destination.resolve(entry.getName());
                if (entry.isDirectory()) {
                    Files.createDirectories(outputPath);
                } else {
                    Files.createDirectories(outputPath.getParent());
                    try (OutputStream outputStream = new FileOutputStream(outputPath.toFile())) {
                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = tais.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("extractTarFile failure: " + e.getMessage() + " " + e.getCause() + " " + e.getClass());
            throw new SrcTarExtractionException(e.getMessage());
        }
    }

    static void deleteDirectory(File directory) {
        System.out.println("deleteDirectory");
        if (directory == null) {
            return;
        }

        if (!directory.exists()) {
            System.out.println("Directory does not exist: " + directory.getPath());
            return;
        }

        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    if (!file.delete()) {
                        System.out.println("Failed to delete file: " + file.getPath());
                    }
                }
            }
        }

        if (!directory.delete()) {
            System.out.println("Failed to delete directory: " + directory.getPath());
        }
    }

    static byte[] createTarFromFiles(ArrayList<String> files, Path sourcePath) throws TarFolderException {
        System.out.println("createTarFromFiles");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (TarArchiveOutputStream tarStream = new TarArchiveOutputStream(baos)) {
            tarStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);

            for (String file : files) {
                Path path = sourcePath.resolve(file);
                Files.walkFileTree(path, EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        String relativePath = sourcePath.relativize(file).toString();
                        TarArchiveEntry entry = new TarArchiveEntry(file.toFile(), "src/" + relativePath);
                        tarStream.putArchiveEntry(entry);
                        try (InputStream is = Files.newInputStream(file)) {
                            IOUtils.copy(is, tarStream);
                        }
                        tarStream.closeArchiveEntry();
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        String relativePath = sourcePath.relativize(dir).toString();
                        TarArchiveEntry entry = new TarArchiveEntry(dir.toFile(), "src/" + relativePath + "/");
                        tarStream.putArchiveEntry(entry);
                        tarStream.closeArchiveEntry();
                        return FileVisitResult.CONTINUE;
                    }
                });
            }
        } catch (Exception e) {
            System.out.println("createTarFromFile failure: " + e.getMessage() + " " + e.getCause() + " " + e.getClass());
            throw new TarFolderException(e.getMessage());
        }
        System.out.println("createTarFromFiles success");
        return baos.toByteArray();
    }

    static void saveXmlToFile(MultipartFile xmlFile, Path targetPath) throws IOException {
        File xmlOutputFile = targetPath.resolve("description.xml").toFile();
        try (InputStream inputStream = xmlFile.getInputStream()) {
            Files.copy(inputStream, xmlOutputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    static boolean processIdAndSaveXml(MultipartFile xmlFile, Path targetPath, Path fragmentsPath) throws Exception {
        File xmlOutputFile = targetPath.resolve("description.xml").toFile();

        try (InputStream inputStream = xmlFile.getInputStream()) {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputStream);
            Element rootElement = document.getRootElement();

            List<Element> codeFragments = rootElement.getChildren("cf");
            for (Element cfElement : codeFragments) {
                if (cfElement.getChild("id") != null) {
                    return false;
                }
            }

            for (Element cfElement : codeFragments) {
                String uniqueId;
                do {
                    uniqueId = UUID.randomUUID().toString();
                } while (findCodeFragmentById(fragmentsPath, uniqueId, null) != null);

                Element idElement = new Element("id").setText(uniqueId);
                cfElement.addContent(idElement);
            }

            try (FileOutputStream outputStream = new FileOutputStream(xmlOutputFile)) {
                XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
                xmlOutputter.output(document, outputStream);
            }
        }
        return true;
    }
}
