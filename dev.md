# Система управления базой фрагментов кода (Библиотека модулей)


 ## ExeStartCProcedure

 - Описание: Плагин конструирует C процедуру запуска фрагмента кода через исполняемый файл.
 - Место запуска процедуры запуска: В той директории где лежит целевая программа (src папка).
 - Функция имеет вид int "Name_of_the_function"(type1 arg1, type2, arg2, type3 arg3, type4 arg4, type5 arg5)
 - Где первые k аргументов входных - входные, с типами const. А оставшиеся n-k аргументов - выходные без типов const. (*Порядок важен, и должен совпадать с тем, что указано в description файле*)
 - Выделение памяти на все аргументы производится вне функции.
 - Возвращается 0 если всё прошло успешно, 1 если нет.

 - *Дополнительные параметры в description файле*:
  - exe_path (локальный путь до исполняемого файла внутри src папки)








**dev**
## Соглашение для нефункциональных свойств (description файл)

description представляет собой xml файл. Пример структуры:

```
<code_fragments>
	<cf>
		<name>Корреляционная свертка сейсмотрасс</name>
		<description>Корреляционная свертка сейсмотрасс. В отчете для полученных коррелотрасс приводится отношение сигнал/шум (SNR) и эквивалентная амплитуда сигнала (A) в дискретах АЦП.Отчет о работе выводится на экран и в файл corr.log В отчете для полученных коррелотрасс приводится отношение сигнал/шум (SNR) и эквивалентная амплитуда сигнала (A) в дискретах АЦП.</description>
		<input_var_count>3</input_var_count>
		<ivars>
			<ivar>Файл сейсмотрассы в формате РС-A</ivar>
			<ivar>Файл опорного сигнала в формате РС-A</ivar>
			<ivar>Интервал корреляции в секундах</ivar>
		</ivars>
		<output_var_count>2</output_var_count>
		<ovars>
			<ovar>Отчет о работе в формате .log</ovar>
			<ovar>Итоговый файл свертки сейсмотрассы</ovar>
		</ovars>
		<ivar_types>
			<ivar_type>any_path</ivar_type>
			<ivar_type>any_path</ivar_type>
			<ivar_type>int</ivar_type>
		</ivar_types>
		<ovar_types>
			<ovar_type>local_path</ovar_type>
			<ovar_type>local_path</ovar_type>
		</ovar_types>
		<local_paths>
			<local_path>corr.log</local_path>
			<local_path>_corr/out</local_path>
		</local_paths>
		<implemented_plugins>
			<plugin>ExeStartCProcedure</plugin>
		</implemented_plugins>
		<plugin_specific>
			<plugin>
				<name>ExeStartCProcedure</name>
				<exe_path>corr.out</exe_path>
			</plugin>
		</plugin_specific>
	</cf>
</code_fragments>
```







Список обязательных свойств:
 - input_var_count
 - output_var_count
 - ivars
 - ovars
 - ivar_types
 - ovar_types
 - implemented_plugins
 
Поддерживаемые типы (в ovar_type и ivar_type) (По надобности расширим)
 - int
 - any_path (при наличии нужно добавить поле "global_paths")
 - local_path (при наличии нужно добавить поле "local_paths")


### Пример использования запроса на добавление:
curl -X POST http://localhost:12345/add_package -H "Content-Type: multipart/form-data" -F "packageName=1_convolution_test_add" -F "xml=@description.xml" -F "file=@test_send.tar"

Для добавления нужен xml файл с description в вышеуказанном формате + tar файл в котором лежит папка src со всем нужным контентом.
