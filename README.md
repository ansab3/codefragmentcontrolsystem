# Система управления базой фрагментов кода (Библиотека модулей)



## Описание работы

Сервер с системой управления базой фрагментов кода запускается на порте: 12345.

При запуске на машине вместе с ядром можно получить доступ через http://localhost:12345/"id_фрагмента_кода"/"Запрос". 

Пример запроса: http://localhost:12345/b642e476-8d79-4783-b1da-a016b0d226aa/info

## Запросы
 
(В этом случае Convolution - id фрагмента кода)
 - code_fragments (http://localhost:12345/code_fragments)
 - info (http://localhost:12345/Convolution/info)
 - plugins (http://localhost:12345/plugins) (http://localhost:12345/Convoltuion/plugins)
 - getNeededContentFor (http://localhost:12345/Convolution/getNeededContentFor?plugin=ExeStartCProcedure)
 - pluginProcedure (http://localhost:12345/Convolution/pluginProcedure?plugin=ExeStartCProcedure)
 - add_package (Тут нужно Post, смотреть ниже)
 - remove_package(Тут нежно Delete, смотреть ниже)

## Пояснения и синтаксис к запросам

 - code_fragments:
   - Описание: Возвращает список [id] фрагментов кода.
   - Возвращаемые значения: json
   - Пример возвращаемого значения: {"code_fragments":["b642e476-8d79-4783-b1da-a016b0d226aa"]}

 - info:
   - Описание: Возвращает всю доступную метаинформацию про фрагмент кода.
   - Возвращаемые значения: json
   - Пример возвращаемого значения: {"inputVarCount":3,"outputVarCount":2,"description":"Корреляционная свертка сейсмотрасс. В отчете для полученных коррелотрасс приводится отношение сигнал/шум (SNR) и эквивалентная амплитуда сигнала (A) в дискретах АЦП.Отчет о работе выводится на экран и в файл corr.log В отчете для полученных коррелотрасс приводится отношение сигнал/шум (SNR) и эквивалентная амплитуда сигнала (A) в дискретах АЦП.","name":"Корреляционная свертка сейсмотрасс","inputVarTypes":["any_path","any_path","int"],"inputVarDescriptions":["Файл сейсмотрассы в формате РС-A","Файл опорного сигнала в формате РС-A","Интервал корреляции в секундах"],"outputVarTypes":["local_path","local_path"],"outputVarDescriptions":["Отчет о работе в формате .log","Итоговый файл свертки сейсмотрассы"],"implementedPlugins":["ExeStartCProcedure"], "package":"Convolution"}
   - Дополнительные параметры: ?type=full (оставить пустым)   ?specific=name (вместо name поставить любой ключ из возврщаемого значения при запросе без specific, также можно комбинировать запросы specific: http://localhost:12345/1_convolution/info?specific=inputVarCount@outputVarCount@description@implementedPlugins)

 - plugins:
   - Описание: Возвращает количество и список поддерживаемых плагинов фрагмента кода. Либо список всех доступных плагинов вообще.
   - Возвращаемые значения: json
   - Пример возвращаемого значения: {"plugins":["ExeStartCProcedure", "getSO"]}

 - getNeededContentFor
   - Описание: Возвращает все нужные корневые файлы фрагмента кода в .tar архиве в зависимости от плагина.
   - Возвращаемые значения: .tar архив
   - Синтаксис возвращаемого значения: Скачивается архив с именем "id фрагмента кода".tar в котором лежит папка src со всеми нужными корневыми файлами фрагмента кода
   - Пример запроса: http://localhost:12345/Convolution/getNeededContentFor?plugin=ExeStartCProcedure
   - Дополнительные параметры: ?plugin=ExeStartCProcedure

 - pluginProcedure
   - Описание: Возвращает результат работы выбранного плагина для выбранного фрагмента кода. (Посмотреть какие плагины поддерживаются данным фрагментом кода можно в info, через поле implementedPlugins)
   - Возвращаемые значения: Зависит от плагина
   - Пример запроса: http://localhost:12345/Convolution/pluginProcedure?plugin=ExeStartCProcedure
   - Возможные значения plugin:
     - ExeStartCProcedure
     - getSO (пока нет)

 - add_package
   - Описание: Добавляет пакет в систему.
   - Входные значения: packageName, MultipartFile (xml, .tar (file)).
   - Post запрос. В xml должен быть description файл. В файле должен быть .tar файл в котором лежит папка src с корневыми файлами фрагмента кода. (curl -X POST http://localhost:12345/add_package -H "Content-Type: multipart/form-data" -F "packageName=Convolution" -F "xml=@description.xml" -F "file=@test_send.tar")

 - remove_package
   - Описание: Удаляет пакет из системы.
   - Входные значения: packageName.
   - Пример запроса (DELETE): curl -X DELETE http://localhost:12345/remove_package?packageName=Convolution

## Соглашение для нефункциональных свойств (description файл)

Поддерживаемые типы (По надобности расширим)
 - int
 - any_path
 - local_path
 
## Коды ошибок 
 - 1 (500) - Ошибка при получении доступа к файлам плагина
 - 2 (500) - Ошибка при получении доступа к description файлу
 - 3 (500) - Неверный формат description файла (internal file corruption или неправильно залили пакет)
 - 4 (500) - Не удалось получить доступ к папке с плагинами
 - 5 (500) - Не удалось создать .tar файл из корневых файлов
 - 6 (500) - Ошибка при получении списка фрагментов кода
 - 7 (500) - Ошибка при получении списка плагинов
 - 8 (400) - Запрошен неизвестный плагин
 - 9 (400) - Запрошен неизвестный запрос 
 - 10 (400) - Запрошен неизвестный метод у фрагмента кода
 - 11 (400) - Запрошен неизвестный фрагмент кода
 - 12 (500) - Не получилось записать значения в description файл при добавлении фрагмента кода
 - 13 (409) - Предосталвенный фрагмент кода уже есть в базе. Измените имя или удалите старый фрагмент кода.
 - 14 (500) - Ошибка при получении доступа к месту хранения фрагментов кода
 - 15 (500) - Ошибка при создании папки для хранения фрагмента кода
 - 16 (500) - Ошибка при распаковке исходных файлов фрагмента кода
 - 17 (400) - В description файле нет запрошенного поля
 - 18 (500) - Неправильный формат плагина
 - 19 (500) - Неизвестная ошибка. Смотерть само сообщение

 Всегда возвращается поле boolean "errorOccured", true - произошла ошибка и тогда будет "errorCode" и "errorMessage":
 {"errorMessage":"There is no such method in the requested code fragment: infa","errorCode":10,"errorOccurred":true}.
 Если же false, то этих полей не будет, и будет ответ на запрос.

Документация для разработчиков [здесь](PLUGINS.md)